/* eslint-disable */
const _ = require('lodash');
const path = require('path');
const { ServiceBroker } = require('moleculer');

ServiceBroker.prototype.loadService = function loadService(filePath) {
  let fName = path.resolve(filePath);
  let schema;

  this.logger.debug(`Load service '${path.basename(fName)}'...`);

  try {
    schema = require(fName);
    if (schema.default) schema = schema.default;
  } catch (e) {
    this.logger.error(`Failed to load service '${fName}'`, e);
  }

  let svc;
  schema = this.normalizeSchemaConstructor(schema);
  if (this.ServiceFactory.isPrototypeOf(schema)) {
    // Service implementation
    svc = new schema(this);

    // If broker is started, call the started lifecycle event of service
    if (this.started) {
      this._restartService(svc);
    }

  } else if (_.isFunction(schema)) {
    // Function
    svc = schema(this);
    if (!(svc instanceof this.ServiceFactory)) {
      svc = this.createService(svc);
    } else {
      // If broker is started, call the started lifecycle event of service
      if (this.started) {
        this._restartService(svc);
      }
    }

  } else if (schema) {
    // Schema object
    svc = this.createService(schema);
  }

  if (svc) {
    svc.__filename = fName;
  }

  if (this.options.hotReload) {
    this.watchService(svc || { __filename: fName, name: fName });
  }

  return svc;
}
